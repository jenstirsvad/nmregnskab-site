# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-03-29 09:21+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: nmregnskab/development_settings.py:127 nmregnskab/settings.py:131
#: templates/default.html:47 templates/products/translation.html:10
#: templates/products/translation.html:19
msgid "English"
msgstr ""

#: nmregnskab/development_settings.py:128 nmregnskab/settings.py:132
#: templates/default.html:46
msgid "Danish"
msgstr ""

#: nmregnskab/urls.py:29
msgid "^products/translation/$"
msgstr ""

#: nmregnskab/urls.py:30
msgid "^pricelist/$"
msgstr ""

#: nmregnskab/urls.py:31
msgid "^about/us/$"
msgstr ""

#: nmregnskab/urls.py:32
msgid "^about/contact/$"
msgstr ""

#: nmregnskab/urls.py:33
msgid "^about/profiles/$"
msgstr ""

#: nmregnskab/views.py:20 templates/about/profiles.html:23
#: templates/navbar.html:31 templates/products/pricelist.html:85
#: templates/products/translation.html:4
msgid "Translation"
msgstr ""

#: nmregnskab/views.py:28 templates/products/pricelist.html:7
msgid "Price list"
msgstr ""

#: nmregnskab/views.py:35 templates/navbar.html:41
msgid "About"
msgstr ""

#: nmregnskab/views.py:43
msgid "Us"
msgstr ""

#: nmregnskab/views.py:51 templates/about/profiles.html:7
msgid "Profiles"
msgstr ""

#: nmregnskab/views.py:59 templates/about/contact.html:4
msgid "Contact us"
msgstr ""

#: templates/about/contact.html:7
msgid "Accountant, translator and conference moderator"
msgstr ""

#: templates/about/contact.html:10
msgid "Accountant and IT support"
msgstr ""

#: templates/about/profile/jtn_profile.inc.html:6
msgid "Correspondence in Danish and English."
msgstr ""

#: templates/about/profile/jtn_profile.inc.html:7
msgid "Monday to Thursday between 9:00 to 12:00"
msgstr ""

#: templates/about/profile/jtn_profile.inc.html:8
msgid "Friday to Sunday between 9:00 to 21:00"
msgstr ""

#: templates/about/profile/jtn_profile.inc.html:9
#: templates/about/profile/nm_profile.inc.html:8
msgid "Phonenumber"
msgstr ""

#: templates/about/profile/nm_profile.inc.html:6
msgid "Correspondence English, Bahasa Malay, Indonesian, Tamil, a bit Danish and little Chinese."
msgstr ""

#: templates/about/profile/nm_profile.inc.html:7
msgid "Monday to Sunday between 09:00 and 21:00"
msgstr ""

#: templates/about/profiles.html:4
msgid "Our staff"
msgstr ""

#: templates/about/profiles.html:10 templates/about/profiles.html:30
msgid "Short intro"
msgstr ""

#: templates/about/profiles.html:15
msgid "Education"
msgstr ""

#: templates/about/profiles.html:16
msgid "2006 Bachelor of business administration (BBA)"
msgstr ""

#: templates/about/profiles.html:17
msgid "See more at my CV"
msgstr ""

#: templates/about/profiles.html:19 templates/about/profiles.html:32
msgid "My expertise"
msgstr ""

#: templates/about/profiles.html:21 templates/about/profiles.html:34
msgid "Bookeeping"
msgstr ""

#: templates/about/profiles.html:22 templates/about/profiles.html:35
#: templates/navbar.html:30 templates/products/pricelist.html:12
#: templates/products/pricelist.html:22
msgid "Accounting"
msgstr ""

#: templates/about/profiles.html:25
msgid "Are availble for project contract and freelance."
msgstr ""

#: templates/about/profiles.html:36
msgid "IT solution and support"
msgstr ""

#: templates/about/us.html:4
msgid "About NM Regnskab"
msgstr ""

#: templates/home.html:33
msgid "Accounting and bookkeeping for small businesses at attractive prices"
msgstr ""

#: templates/home.html:38
msgid "Help for bookkeeping and accounting"
msgstr ""

#: templates/home.html:39
msgid "Make what you do best and let us do what we do best."
msgstr ""

#: templates/home.html:42
msgid "Bookkeeping"
msgstr ""

#: templates/home.html:43
msgid "We will convert your piles of annex into a final balance sheet and give you an overview of your company's finances / accounts."
msgstr ""

#: templates/home.html:44
msgid "We take care of your bookkeeping and ensure you have a complete overview of the economy."
msgstr ""

#: templates/home.html:44
msgid "We are in control of rules and legal requirements and ensure timely reporting to authorities."
msgstr ""

#: templates/home.html:45
msgid "We offer you your account so you can understand it and at the pace we agree."
msgstr ""

#: templates/home.html:46
msgid "When we make the accounts, it is done on the accounting program in the cloud."
msgstr ""

#: templates/home.html:47
msgid "Debtors management"
msgstr ""

#: templates/home.html:48
msgid "Checking billing vs payment"
msgstr ""

#: templates/home.html:49
msgid "We offer to take over your invoicing, so that you save both time and quicker receive money from debtors."
msgstr ""

#: templates/home.html:50
msgid "Creditors management"
msgstr ""

#: templates/home.html:51
msgid "VAT accounting"
msgstr ""

#: templates/home.html:52
msgid "Payroll administration"
msgstr ""

#: templates/home.html:53
msgid "We administrates salary, holiday allowance, pension, etc"
msgstr ""

#: templates/home.html:53
msgid "All you have to do is keep an account of the number of hours that employees have worked."
msgstr ""

#: templates/home.html:54
msgid "We have solutions where the employee himself stamps in and out."
msgstr ""

#: templates/home.html:54
msgid "All you have to do is approve the hours."
msgstr ""

#: templates/navbar.html:28 templates/products/pricelist.html:15
#: templates/products/pricelist.html:78 templates/products/pricelist.html:100
#: templates/products/pricelist.html:117
msgid "Products"
msgstr ""

#: templates/navbar.html:32
msgid "Pricelist"
msgstr ""

#: templates/navbar.html:34 templates/products/freelance.html:4
msgid "Freelance"
msgstr ""

#: templates/navbar.html:39
msgid "About us"
msgstr ""

#: templates/navbar.html:42
msgid "Who are we"
msgstr ""

#: templates/navbar.html:43
msgid "Contact"
msgstr ""

#: templates/navbar.html:54
msgid "profile"
msgstr ""

#: templates/navbar.html:55
msgid "settings"
msgstr ""

#: templates/navbar.html:56
msgid "logout"
msgstr ""

#: templates/navbar.html:60
msgid "administration"
msgstr ""

#: templates/navbar.html:63
msgid "login"
msgstr ""

#: templates/products/accounting.html:4
msgid "Accounting services"
msgstr ""

#: templates/products/pricelist.html:4
msgid "pricelist 2018"
msgstr ""

#: templates/products/pricelist.html:4
msgid " updated april"
msgstr ""

#: templates/products/pricelist.html:8
msgid "All prices are ex. VAT"
msgstr ""

#: templates/products/pricelist.html:17 templates/products/pricelist.html:80
#: templates/products/pricelist.html:102 templates/products/pricelist.html:119
msgid "units"
msgstr ""

#: templates/products/pricelist.html:24 templates/products/pricelist.html:87
#: templates/products/pricelist.html:109
msgid "per hour"
msgstr ""

#: templates/products/pricelist.html:27
msgid "Extended personal tax declaration due business tax scheme"
msgstr ""

#: templates/products/pricelist.html:29
msgid "per year"
msgstr ""

#: templates/products/pricelist.html:32
msgid "The package deal tiny (200 annex) *"
msgstr ""

#: templates/products/pricelist.html:34 templates/products/pricelist.html:39
#: templates/products/pricelist.html:44 templates/products/pricelist.html:49
#: templates/products/pricelist.html:54
msgid "annually"
msgstr ""

#: templates/products/pricelist.html:37
msgid "The package deal small (400 annex)"
msgstr ""

#: templates/products/pricelist.html:42
msgid "The package deal standard (1000 annex) *"
msgstr ""

#: templates/products/pricelist.html:47
msgid "The package deal large (1600 annex)"
msgstr ""

#: templates/products/pricelist.html:52
msgid "For every additional 200 annex when using package deal (large)"
msgstr ""

#: templates/products/pricelist.html:58
msgid "* Package deal"
msgstr ""

#: templates/products/pricelist.html:59
msgid "Included"
msgstr ""

#: templates/products/pricelist.html:60
msgid "Recording  of vouchers"
msgstr ""

#: templates/products/pricelist.html:61
msgid "Ongoing revision of all financial items"
msgstr ""

#: templates/products/pricelist.html:62
msgid "Ongoing advice and sparring about your accounts"
msgstr ""

#: templates/products/pricelist.html:63
msgid "VAT reporting to SKAT"
msgstr ""

#: templates/products/pricelist.html:64
msgid "Annual Report"
msgstr ""

#: templates/products/pricelist.html:65
msgid "Meeting year-end accounting"
msgstr ""

#: templates/products/pricelist.html:66
msgid "Requirements"
msgstr ""

#: templates/products/pricelist.html:67
msgid "Properly sorting of vouchers"
msgstr ""

#: templates/products/pricelist.html:68
msgid "Vouchers scanned as jpeg or pdf file"
msgstr ""

#: templates/products/pricelist.html:69
msgid "Bank account statement as cvs file"
msgstr ""

#: templates/products/pricelist.html:75
msgid "Administration"
msgstr ""

#: templates/products/pricelist.html:90
msgid "Sending invoices by letter"
msgstr ""

#: templates/products/pricelist.html:92
msgid "per letter"
msgstr ""

#: templates/products/pricelist.html:97
msgid "IT Solutions"
msgstr ""

#: templates/products/pricelist.html:107
msgid "IT support"
msgstr ""

#: templates/products/pricelist.html:114
msgid "External collaborators"
msgstr ""

#: templates/products/pricelist.html:123
msgid "Minimal account<br>(ltd, private limited company and entrepreneurial company) Class A and B accounts"
msgstr ""

#: templates/products/pricelist.html:125 templates/products/pricelist.html:131
msgid "year"
msgstr ""

#: templates/products/pricelist.html:128
msgid "Double account<br>Operating + Holding accounts. (ltd, private limited company and entrepreneurial company) Class A and B accounts"
msgstr ""

#: templates/products/pricelist.html:134
msgid "Note that we can not perform audits in companies. If you have a company that requires audit, we assist in preparing accounting for a state-authorized accountant."
msgstr ""

#: templates/products/pricelist.html:136
msgid "See the rules here"
msgstr ""

#: templates/products/pricelist.html:140
msgid "Betingelser for at fravælge revision i IVS, APS"
msgstr ""

#: templates/products/pricelist.html:141
msgid "Virksomheder kan fravælge revision, hvis de to regnskabsår i træk ikke overskrider to af følgende tre størrelser på balancedagen:"
msgstr ""

#: templates/products/pricelist.html:143
msgid "en balancesum på 4 mio. kr."
msgstr ""

#: templates/products/pricelist.html:144
msgid "en nettoomsætning på 8 mio. kr. og"
msgstr ""

#: templates/products/pricelist.html:145
msgid "et gennemsnitligt antal heltidsbeskæftigede på 12 i løbet af regnskabsåret"
msgstr ""

#: templates/products/pricelist.html:146
msgid "Nystiftede virksomheder kan efter det første regnskabsår fravælge revision af årsrapporten, hvis de ikke på balancedagen for det første regnskabsår overskrider to af de tre ovennævnte størrelsesgrænser."
msgstr ""

#: templates/products/pricelist.html:148
msgid "Ligeledes kan virksomheder fravælge revision allerede ved stiftelsen."
msgstr ""

#: templates/products/pricelist.html:150
msgid "Erhvervsdrivende fonde kan ikke fravælge revision."
msgstr ""

#: templates/products/translation.html:6
msgid "Oral translation"
msgstr ""

#: templates/products/translation.html:8 templates/products/translation.html:17
msgid "Bahasa Malay"
msgstr ""

#: templates/products/translation.html:9
msgid "Indonesia"
msgstr ""

#: templates/products/translation.html:11
msgid "Tamil"
msgstr ""

#: templates/products/translation.html:12
msgid "Little Chinese Hokkien"
msgstr ""

#: templates/products/translation.html:13
msgid "Little Danish"
msgstr ""

#: templates/products/translation.html:15
msgid "Writing translation"
msgstr ""

#: templates/products/translation.html:18
msgid "Indonesian"
msgstr ""
